/* bs-icon.c
 *
 * Copyright 2022 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "bs-icon.h"

struct _BsIcon
{
  GObject parent_instance;

  GdkRGBA background_color;
  GdkPaintable *paintable;
  PangoLayout *layout;
  GFile *file;
  char *icon_name;
  int margin;
  BsIcon *relative;

  GtkIconPaintable *icon_paintable;
  GdkTexture *file_texture;

  gulong relative_size_changed_id;
  gulong relative_content_changed_id;

  gulong size_changed_id;
  gulong content_changed_id;
};

static void gdk_paintable_iface_init (GdkPaintableInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (BsIcon, bs_icon, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (GDK_TYPE_PAINTABLE, gdk_paintable_iface_init))

enum
{
  PROP_0,
  PROP_BACKGROUND_COLOR,
  PROP_FILE,
  PROP_ICON_NAME,
  PROP_MARGIN,
  PROP_PAINTABLE,
  PROP_RELATIVE,
  PROP_TEXT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * Callbacks
 */

static void
on_paintable_contents_changed_cb (GdkPaintable *paintable,
                                  BsIcon       *self)
{
  gdk_paintable_invalidate_contents (GDK_PAINTABLE (self));
}

static void
on_paintable_size_changed_cb (GdkPaintable *paintable,
                              BsIcon       *self)
{
  gdk_paintable_invalidate_size (GDK_PAINTABLE (self));
}

static gboolean
snapshot_any_paintable (GdkSnapshot *snapshot,
                        BsIcon      *icon,
                        double       width,
                        double       height)
{
  gboolean painted = FALSE;

  gtk_snapshot_save (snapshot);
  gtk_snapshot_translate (snapshot, &GRAPHENE_POINT_INIT (icon->margin, icon->margin));

  if (icon->paintable)
    {
      gdk_paintable_snapshot (icon->paintable,
                              snapshot,
                              width - icon->margin * 2,
                              height - icon->margin * 2);
      painted = TRUE;
    }
  else if (icon->file_texture)
    {
      gdk_paintable_snapshot (GDK_PAINTABLE (icon->file_texture),
                              snapshot,
                              width - icon->margin * 2,
                              height - icon->margin * 2);
      painted = TRUE;
    }
  else if (icon->icon_paintable)
    {
      gdk_paintable_snapshot (GDK_PAINTABLE (icon->icon_paintable),
                              snapshot,
                              width - icon->margin * 2,
                              height - icon->margin * 2);

      painted = TRUE;
    }

  gtk_snapshot_restore (snapshot);

  return painted;
}

static gboolean
snapshot_any_layout (GdkSnapshot *snapshot,
                     BsIcon      *icon,
                     double       width,
                     double       height)
{
  if (icon->layout)
    {
      int text_width, text_height;
      int x, y;

      pango_layout_get_pixel_size (icon->layout, &text_width, &text_height);
      x = (width - text_width) / 2.0;
      y = height - text_height;

      gtk_snapshot_save (snapshot);
      gtk_snapshot_translate (snapshot, &GRAPHENE_POINT_INIT (x, y));
      gtk_snapshot_append_layout (snapshot, icon->layout, &(GdkRGBA) { 1.0, 1.0, 1.0, 1.0 });
      gtk_snapshot_restore (snapshot);
    }

  return icon->layout != NULL;
}


/*
 * GdkPaintable interface
 */

static double
bs_icon_get_intrinsic_aspect_ratio (GdkPaintable *paintable)
{
  BsIcon *self = BS_ICON (paintable);

  return self->paintable ? gdk_paintable_get_intrinsic_aspect_ratio (self->paintable) : 1.0;
}

static int
bs_icon_get_intrinsic_width (GdkPaintable *paintable)
{
  BsIcon *self = BS_ICON (paintable);

  return self->paintable ? gdk_paintable_get_intrinsic_width (self->paintable) : 0;
}


static int
bs_icon_get_intrinsic_height (GdkPaintable *paintable)
{
  BsIcon *self = BS_ICON (paintable);

  return self->paintable ? gdk_paintable_get_intrinsic_height (self->paintable) : 0;
}

static void
bs_icon_snapshot (GdkPaintable *paintable,
                  GdkSnapshot  *snapshot,
                  double        width,
                  double        height)
{
  BsIcon *self = BS_ICON (paintable);

  gtk_snapshot_append_color (snapshot,
                             &self->background_color,
                             &GRAPHENE_RECT_INIT (0, 0, width, height));

  if (!snapshot_any_paintable (snapshot, self, width, height) && self->relative)
    snapshot_any_paintable (snapshot, self->relative, width, height);

  if (!snapshot_any_layout (snapshot, self, width, height) && self->relative)
    snapshot_any_layout (snapshot, self->relative, width, height);
}

static void
gdk_paintable_iface_init (GdkPaintableInterface *iface)
{
  iface->get_intrinsic_aspect_ratio = bs_icon_get_intrinsic_aspect_ratio;
  iface->get_intrinsic_height = bs_icon_get_intrinsic_height;
  iface->get_intrinsic_width = bs_icon_get_intrinsic_width;
  iface->snapshot = bs_icon_snapshot;
}


/*
 * GObject overrides
 */

static void
bs_icon_finalize (GObject *object)
{
  BsIcon *self = (BsIcon *)object;

  g_clear_signal_handler (&self->relative_content_changed_id, self->relative);
  g_clear_signal_handler (&self->relative_size_changed_id, self->relative);
  g_clear_signal_handler (&self->content_changed_id, self->paintable);
  g_clear_signal_handler (&self->size_changed_id, self->paintable);
  g_clear_pointer (&self->icon_name, g_free);
  g_clear_object (&self->paintable);
  g_clear_object (&self->file_texture);
  g_clear_object (&self->file);
  g_clear_object (&self->layout);

  G_OBJECT_CLASS (bs_icon_parent_class)->finalize (object);
}

static void
bs_icon_get_property (GObject    *object,
                      guint       prop_id,
                      GValue     *value,
                      GParamSpec *pspec)
{
  BsIcon *self = BS_ICON (object);

  switch (prop_id)
    {
    case PROP_BACKGROUND_COLOR:
      g_value_set_boxed (value, &self->background_color);
      break;

    case PROP_FILE:
      g_value_set_object (value, self->file);
      break;

    case PROP_ICON_NAME:
      g_value_set_string (value, self->icon_name);
      break;

    case PROP_MARGIN:
      g_value_set_int (value, self->margin);
      break;

    case PROP_PAINTABLE:
      g_value_set_object (value, self->paintable);
      break;

    case PROP_RELATIVE:
      g_value_set_object (value, self->relative);
      break;

    case PROP_TEXT:
      g_value_set_string (value, self->layout ? pango_layout_get_text (self->layout) : NULL);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bs_icon_set_property (GObject      *object,
                      guint         prop_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
  BsIcon *self = BS_ICON (object);

  switch (prop_id)
    {
    case PROP_BACKGROUND_COLOR:
      bs_icon_set_background_color (self, g_value_get_boxed (value));
      break;

    case PROP_FILE:
      bs_icon_set_file (self, g_value_get_object (value), NULL);
      break;

    case PROP_ICON_NAME:
      bs_icon_set_icon_name (self, g_value_get_string (value));
      break;

    case PROP_MARGIN:
      bs_icon_set_margin (self, g_value_get_int (value));
      break;

    case PROP_PAINTABLE:
      bs_icon_set_paintable (self, g_value_get_object (value));
      break;

    case PROP_RELATIVE:
      bs_icon_set_relative (self, g_value_get_object (value));
      break;

    case PROP_TEXT:
      bs_icon_set_text (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bs_icon_class_init (BsIconClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = bs_icon_finalize;
  object_class->get_property = bs_icon_get_property;
  object_class->set_property = bs_icon_set_property;

  properties[PROP_BACKGROUND_COLOR] = g_param_spec_boxed ("background-color", NULL, NULL,
                                                          GDK_TYPE_RGBA,
                                                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_FILE] = g_param_spec_object ("file", NULL, NULL,
                                               G_TYPE_FILE,
                                               G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_ICON_NAME] = g_param_spec_string ("icon-name", NULL, NULL,
                                                    NULL,
                                                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_MARGIN] = g_param_spec_int ("margin", NULL, NULL,
                                              0, G_MAXINT, 12,
                                              G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_PAINTABLE] = g_param_spec_object ("paintable", NULL, NULL,
                                                    GDK_TYPE_PAINTABLE,
                                                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_RELATIVE] = g_param_spec_object ("relative", NULL, NULL,
                                                    BS_TYPE_ICON,
                                                    G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_TEXT] = g_param_spec_string ("text", NULL, NULL,
                                               NULL,
                                               G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bs_icon_init (BsIcon *self)
{
  self->margin = 12;
}


BsIcon *
bs_icon_new_empty (void)
{
  GdkRGBA transparent = (GdkRGBA) { 0.0, 0.0, 0.0, 0.0 };

  return bs_icon_new (&transparent, NULL);
}

BsIcon *
bs_icon_new (const GdkRGBA *background_color,
             GdkPaintable  *paintable)
{
  return g_object_new (BS_TYPE_ICON,
                       "background-color", background_color,
                       "paintable", paintable,
                       NULL);
}

BsIcon *
bs_icon_new_from_json (JsonNode  *node,
                       GError   **error)
{
  g_autoptr (GFile) file = NULL;
  JsonObject *object;
  GdkRGBA background_color;

  if (!JSON_NODE_HOLDS_OBJECT (node))
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED, "JSON node is not an object");
      return NULL;
    }

  object = json_node_get_object (node);
  gdk_rgba_parse (&background_color,
                  json_object_get_string_member_with_default (object,
                                                              "background-color",
                                                              "rgba(0,0,0,0)"));

  if (json_object_has_member (object, "file"))
    file = g_file_new_for_uri (json_object_get_string_member (object, "file"));

  return g_object_new (BS_TYPE_ICON,
                       "background-color", &background_color,
                       "file", file,
                       "icon-name", json_object_get_string_member_with_default (object, "icon-name", NULL),
                       "text", json_object_get_string_member_with_default (object, "text", NULL),
                       "margin", json_object_get_int_member_with_default (object, "margin", 12),
                       NULL);
}

JsonNode *
bs_icon_to_json (BsIcon *self)
{
  g_autoptr (JsonBuilder) builder = NULL;
  g_autofree char *background_color = NULL;

  g_return_val_if_fail (BS_IS_ICON (self), NULL);

  builder = json_builder_new ();

  json_builder_begin_object (builder);

  background_color = gdk_rgba_to_string (&self->background_color);

  json_builder_set_member_name (builder, "background-color");
  json_builder_add_string_value (builder, background_color);

  json_builder_set_member_name (builder, "margin");
  json_builder_add_int_value (builder, self->margin);

  json_builder_set_member_name (builder, "text");
  json_builder_add_string_value (builder, self->layout ? pango_layout_get_text (self->layout) : "");

  if (self->file)
    {
      g_autofree char *uri = g_file_get_uri (self->file);

      json_builder_set_member_name (builder, "file");
      json_builder_add_string_value (builder, uri);
    }

  if (self->icon_name)
    {
      json_builder_set_member_name (builder, "icon-name");
      json_builder_add_string_value (builder, self->icon_name);
    }

  json_builder_end_object (builder);

  return json_builder_get_root (builder);
}

const GdkRGBA *
bs_icon_get_background_color (BsIcon *self)
{
  g_return_val_if_fail (BS_IS_ICON (self), NULL);

  return &self->background_color;
}

void
bs_icon_set_background_color (BsIcon        *self,
                              const GdkRGBA *background_color)
{
  g_return_if_fail (BS_IS_ICON (self));

  if (background_color)
    self->background_color = *background_color;
  else
    self->background_color = (GdkRGBA) { 0.0, 0.0, 0.0, 0.0 };

  gdk_paintable_invalidate_contents (GDK_PAINTABLE (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_BACKGROUND_COLOR]);
}

GFile *
bs_icon_get_file (BsIcon *self)
{
  g_return_val_if_fail (BS_IS_ICON (self), NULL);

  return self->file;
}

void
bs_icon_set_file (BsIcon  *self,
                  GFile   *file,
                  GError **error)
{
  g_autoptr (GdkTexture) texture = NULL;

  g_return_if_fail (BS_IS_ICON (self));

  if (self->file == file)
    return;

  if (file)
    {
      texture = gdk_texture_new_from_file (file, error);
      if (!texture)
        return;
    }

  g_set_object (&self->file_texture, texture);
  g_set_object (&self->file, file);

  gdk_paintable_invalidate_contents (GDK_PAINTABLE (self));
  gdk_paintable_invalidate_size (GDK_PAINTABLE (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_FILE]);
}

const char *
bs_icon_get_icon_name (BsIcon *self)
{
  g_return_val_if_fail (BS_IS_ICON (self), NULL);

  return self->icon_name;
}

void
bs_icon_set_icon_name (BsIcon     *self,
                       const char *icon_name)
{
  GtkIconTheme *icon_theme;

  g_return_if_fail (BS_IS_ICON (self));

  if (g_strcmp0 (self->icon_name, icon_name) == 0)
    return;

  g_clear_pointer (&self->icon_name, g_free);
  self->icon_name = g_strdup (icon_name);

  icon_theme = gtk_icon_theme_get_for_display (gdk_display_get_default ());
  self->icon_paintable = gtk_icon_theme_lookup_icon (icon_theme,
                                                     icon_name,
                                                     NULL,
                                                     96,
                                                     1,
                                                     GTK_TEXT_DIR_RTL,
                                                     0);

  gdk_paintable_invalidate_contents (GDK_PAINTABLE (self));
  gdk_paintable_invalidate_size (GDK_PAINTABLE (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ICON_NAME]);
}

GdkPaintable *
bs_icon_get_paintable (BsIcon *self)
{
  g_return_val_if_fail (BS_IS_ICON (self), NULL);

  return self->paintable;
}

void
bs_icon_set_paintable (BsIcon       *self,
                       GdkPaintable *paintable)
{
  g_return_if_fail (BS_IS_ICON (self));
  g_return_if_fail (paintable != GDK_PAINTABLE (self));

  if (self->paintable == paintable)
    return;

  g_clear_signal_handler (&self->content_changed_id, self->paintable);
  g_clear_signal_handler (&self->size_changed_id, self->paintable);

  g_set_object (&self->paintable, paintable);

  self->content_changed_id = g_signal_connect (paintable,
                                               "invalidate-contents",
                                               G_CALLBACK (on_paintable_contents_changed_cb),
                                               self);

  self->size_changed_id = g_signal_connect (paintable,
                                            "invalidate-contents",
                                            G_CALLBACK (on_paintable_size_changed_cb),
                                            self);

  gdk_paintable_invalidate_contents (GDK_PAINTABLE (self));
  gdk_paintable_invalidate_size (GDK_PAINTABLE (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_PAINTABLE]);
}

const char *
bs_icon_get_text (BsIcon *self)
{
  g_return_val_if_fail (BS_IS_ICON (self), NULL);

  return self->layout ? pango_layout_get_text (self->layout) : NULL;
}

void
bs_icon_set_text (BsIcon     *self,
                  const char *text)
{
  g_return_if_fail (BS_IS_ICON (self));

  if (self->layout && g_strcmp0 (pango_layout_get_text (self->layout), text) == 0)
    return;

  if (text)
    {
      if (!self->layout)
        {
          g_autoptr (PangoFontDescription) font_description = NULL;
          PangoContext *pango_context;

          font_description = pango_font_description_from_string ("Cantarell 10");

          pango_context = pango_font_map_create_context (pango_cairo_font_map_get_default ());
          pango_context_set_language (pango_context, gtk_get_default_language ());
          pango_context_set_font_description (pango_context, font_description);

          self->layout = pango_layout_new (pango_context);

          g_clear_object (&pango_context);
        }

      pango_layout_set_text (self->layout, text, -1);
    }
  else
    {
      g_clear_object (&self->layout);
    }

  gdk_paintable_invalidate_contents (GDK_PAINTABLE (self));
  gdk_paintable_invalidate_size (GDK_PAINTABLE (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_TEXT]);
}

int
bs_icon_get_margin (BsIcon *self)
{
  g_return_val_if_fail (BS_IS_ICON (self), 0);

  return self->margin;
}

void
bs_icon_set_margin (BsIcon *self,
                    int     margin)
{
  g_return_if_fail (BS_IS_ICON (self));

  if (self->margin == margin)
    return;

  self->margin = margin;

  gdk_paintable_invalidate_contents (GDK_PAINTABLE (self));
  gdk_paintable_invalidate_size (GDK_PAINTABLE (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_MARGIN]);
}

BsIcon *
bs_icon_get_relative (BsIcon *self)
{
  g_return_val_if_fail (BS_IS_ICON (self), NULL);

  return self->relative;
}

void
bs_icon_set_relative (BsIcon *self,
                      BsIcon *relative)
{
  g_return_if_fail (BS_IS_ICON (self));
  g_return_if_fail (relative == NULL || BS_IS_ICON (relative));
  g_return_if_fail (relative != self);

  if (self->relative == relative)
    return;

  g_clear_signal_handler (&self->relative_content_changed_id, self->relative);
  g_clear_signal_handler (&self->relative_size_changed_id, self->relative);

  self->relative = relative;

  if (relative)
    {
      self->relative_content_changed_id = g_signal_connect (relative,
                                                            "invalidate-contents",
                                                            G_CALLBACK (on_paintable_contents_changed_cb),
                                                            self);

      self->relative_size_changed_id = g_signal_connect (relative,
                                                         "invalidate-contents",
                                                         G_CALLBACK (on_paintable_size_changed_cb),
                                                         self);

    }

  gdk_paintable_invalidate_contents (GDK_PAINTABLE (self));
  gdk_paintable_invalidate_size (GDK_PAINTABLE (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_RELATIVE]);
}
